import * as React from 'react';
import NavBar from '../components/NavBar';
import Container from '../components/Container';
import ContactForm from '../components/ContactForm';
import Footer from '../components/Footer';

const ContactPage = () => {
  return (
    <>
      <NavBar />
      <section className="bg-gray-100 py-32">
        <Container>
          <div className="grid grid-cols-1 gap-20 md:grid-cols-2">
            <div className="col-span-1 gap-4 md:col-span-1">
              <h1 className="text-3xl font-bold mb-4">Contact Us</h1>
              <p>If you have any questions or comments, please feel free to reach out to us.</p>
              <ul className="mt-4">
                <li className="flex items-center py-2">
                  <svg
                    className="w-6 h-6 mr-4"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8"></path>
                  </svg>
                  <a href="mailto:test@gmail.com">
                    <span className="text-gray-600 hover:underline">
                      test@gmail.com
                    </span>
                  </a>
                </li>
                <li className="flex items-center py-2">
                  <svg
                    className="w-6 h-6 mr-4"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8"></path>
                  </svg>
                  <a href="tel:123-456-7890">
                    <span className="text-gray-600 hover:underline">
                      123-456-7890
                    </span>
                  </a>
                </li>
                <li className="flex items-center py-2">
                  <svg
                    className="w-6 h-6 mr-4"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8"></path>
                  </svg>
                  <span className="text-gray-600 hover:underline">
                    123 Main Street
                  </span>
                </li>
                </ul>
            </div>
            <div className="col-span-2 md:col-span-1">
              <ContactForm />
            </div>
          </div>
        </Container>
      </section>
      <Footer />
    </>
  );
};

export default ContactPage;
