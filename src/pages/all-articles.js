import * as React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import Navbar from '../components/NavBar'
import Footer from '../components/Footer'
import Container from '../components/Container'
import ArticleCard from '../components/ArticleCard'


const AllArticlesPage = () => {
  const data = useStaticQuery(query)
  const articles = data.allDatoCmsArticle.nodes

  return (
    <main>
      <Navbar />
      <Container>
        <section className="py-32">
        <h1 className="text-4xl font-bold mb-4">All Articles</h1>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            {articles.map((article) => (
              <ArticleCard
                key={article.slug}
                title={article.title}
                coverImage={article.image.url}
                date={article.meta.publishedAt}
                slug={article.slug} 
                authorName={article.author.name}  
                authorPhotoUrl={article.author.authorPhoto.url}
              />
            ))}
          </div> 
        </section>
      </Container>
      <Footer />
    </main>
  )
}

export const query = graphql`
  query {
    allDatoCmsArticle(sort: { fields: [meta___publishedAt], order: DESC }) {
      nodes {
        slug
        title
        author {
          name
          authorPhoto {
            url
          }
        }
        image {
          url
        }
        category {
          name
        }
        meta {
          publishedAt(formatString: "DD MMMM, YYYY")
        }
      }
    }
  }
`

export const Head = () => <title>Max Blog - Home</title>


export default AllArticlesPage

  