import * as React from 'react'
import { graphql } from 'gatsby'
import Navbar from '../../components/NavBar'
import Footer from '../../components/Footer'
import Container from '../../components/Container'
import Button from '../../components/Button'
import ReactMarkdown from 'react-markdown'
import Author from '../../components/Author'
import ArticleCard from '../../components/ArticleCard'

export const query = graphql`
  query ArticleQuery($slug: String!) {
    datoCmsArticle(slug: { eq: $slug }) {
      slug
      title
      content
      author {
        name
        authorPhoto {
          url
        }
      }
      image {
        url
      }
      category {
        name
      }
      readMore {
        ... on DatoCmsArticle {
          slug
          title
          image {
            url
          }
          category {
            name
          }
          meta {
            publishedAt(formatString: "DD MMMM, YYYY")
          }
          author {
            name
            authorPhoto {
              url
            }
          }
        }
      }
      meta {
        publishedAt(formatString: "DD MMMM, YYYY")
      }
    }
  }
`

const ArticleTemplate = ({ data }) => {
  const article = data.datoCmsArticle

  return (
    <main>
      <Navbar />
      <section className="hero bg-gray-200 py-32">
      <Container>
        <article>
        <div className="prose max-w-2xl lg:prose-xl mb-6 mx-auto">
          <h1 className="text-5xl font-bold leading-tight mb-4">{article.title}</h1>
          <p className="text-sm text-gray-500 mb-2">{article.meta.publishedAt}</p>
          <span className="bg-green-500 text-white text-xs font-semibold mr-2 px-2.5 py-0.5 rounded">{article.category.name}</span>
        </div>
          <div className="mb-4">
            <img
              src={article.image.url}
              alt={article.title}
              className="object-cover w-full h-96"
            />
          </div>
          <div className="prose max-w-2xl lg:prose-xl mb-6 mx-auto">
            <ReactMarkdown>{article.content}</ReactMarkdown>
          </div>
          <div className="prose max-w-2xl lg:prose-xl mb-6 mx-auto">
            <Author name={article.author.name} photoUrl={article.author.authorPhoto.url} />
            <Button to="/all-articles">Back to Articles</Button> 
          </div>
        </article>
      </Container>
        </section>
        <section className="py-8">
          <Container>
            <h2 className="text-3xl font-bold mb-4">Read More Articles</h2>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
              {article.readMore.map((article) => (
                <ArticleCard
                  key={article.slug}
                  title={article.title}
                  coverImage={article.image.url}
                  date={article.meta.publishedAt}
                  slug={article.slug}   
                  authorName={article.author.name}
                  authorPhotoUrl={article.author.authorPhoto.url}            
                />
              ))}
            </div>
          </Container>
        </section>
      <Footer />
    </main>
  )
}

export default ArticleTemplate
