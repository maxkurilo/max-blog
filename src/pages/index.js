import * as React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import Navbar from '../components/NavBar'
import Container from '../components/Container'
import Footer from '../components/Footer'
import Button from '../components/Button'
import ArticleCard from '../components/ArticleCard'



const IndexPage = () => {
  const data = useStaticQuery(query)
  const articles = data.allDatoCmsArticle.nodes

  return (
    <main>
      <Navbar />
      <section className="hero bg-gray-200 py-32">
      <Container>
          <h1 className="text-5xl font-bold leading-tight mb-4">Welcome to Our Blog</h1>
          <p className="text-xl mb-8">Explore a wealth of knowledge on various topics written by our expert authors.</p>
          <Button to="/all-articles">View Articles</Button>
        </Container>
      </section>
      <Container>
        <section className="py-32">
        <h1 className="text-4xl font-bold mb-4">Latest Articles</h1>
          <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8">
            {articles.map((article) => (
              <ArticleCard
                key={article.slug}
                title={article.title}
                coverImage={article.image.url}
                date={article.meta.publishedAt}
                slug={article.slug} 
                authorName={article.author.name}
                authorPhotoUrl={article.author.authorPhoto.url}                
              />
            ))} 
          </div> 
          <div className="flex justify-center mt-8">
            <Button to="/all-articles">View All Articles</Button>
          </div>
        </section>
      </Container>
      <Footer />
    </main>
  )
}


export const query = graphql`
  query {
    allDatoCmsArticle(sort: { fields: [meta___publishedAt], order: DESC }, limit: 3) {
      nodes {
        slug
        title
        author {
          name
          authorPhoto {
            url
          }
        }
        image {
          url
        }
        category {
          name
        }
        meta {
          publishedAt(formatString: "DD MMMM, YYYY")
        }
      }
    }
  }
`

export const Head = () => <title>Max Blog - Home</title>


export default IndexPage
