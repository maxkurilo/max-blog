import * as React from 'react';
import NavBar from '../components/NavBar';
import Container from '../components/Container';
import Footer from '../components/Footer';


const AboutUsPage = () => {
  return (
    <div>
      <NavBar />
    <section className="bg-gray-100 py-32 min-h-screen">
      <Container>
        <h1 className="text-3xl font-bold mb-4">About Us</h1>
        <p>Welcome to our About Us page. Here you can learn more about our mission, values, and the team behind our blog.</p>
        <p>Our blog is dedicated to providing you with the latest news, insights, and tips in the industry. We strive to create content that is not only informative but also engaging and thought-provoking.</p>
        <p>If you have any questions or would like to know more about us, please don't hesitate to reach out.</p>
      </Container>
    </section>
    <Footer />
    </div>
  );
};

export default AboutUsPage;
