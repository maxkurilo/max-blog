import * as React from 'react';

const Container = ({children}) => {
    return (
        <div className="container mx-auto max-w-5xl px-5">
            {children}
        </div>
    )
}

export default Container;