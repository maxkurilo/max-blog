import React from "react";
import { Link } from "gatsby";
import Author from "./Author";

const PostPreview = ({ title, date, slug, coverImage, authorName, authorPhotoUrl }) => (
  <div>
    <Link to={`/articles/${slug}`} className="mb-5">
      <img className="object-cover h-48 w-full " src={coverImage} alt={title} />
    </Link>
    <h3 className="text-xl font-bold mb-3 leading-snug">
      <Link to={`/articles/${slug}`}>{title}</Link>
    </h3>
    <Author name={authorName} photoUrl={authorPhotoUrl} />
      <p className="text-sm text-gray-500 mb-2">{date}</p>
  </div>
);

export default PostPreview;