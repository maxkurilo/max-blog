import * as React from 'react';
import { Link } from 'gatsby';

const Button = ({ variant, children, to }) => {
  const baseStyle = 'px-4 py-2 rounded focus:outline-none focus:shadow-outline';
  const variantStyles = {
    default: 'inline-block bg-slate-600 text-white font-bold py-2 px-4 rounded',
    secondary: 'inline-block bg-indigo-500 text-white font-bold py-2 px-4 rounded'
  };

  const style = `${baseStyle} ${variantStyles[variant] || variantStyles.default}`;

  return (
    <Link to={to} className={style}>
      {children}
    </Link>
  );
};

export default Button;
