import * as React from 'react'; 

const Footer = () => {
    return (
        <footer className="bg-gray-800 py-20">
            <div className="container mx-auto px-6">
                <div className="flex flex-col items-center">
                    <div className="sm:w-2/3 text-center">
                        <p className="text-sm text-white font-bold">
                            © 2024 by Gatsby and Dato CMS Blog
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer;
