import * as React from 'react';

const Author = ({ name, photoUrl }) => {
  return (
    <div className="flex items-center mb-4">
      <img
        className="h-10 w-10 rounded-full mr-2"
        src={photoUrl}
        alt={name}
      />
      <span className="text-lg">{name}</span>
    </div>
  );
};


export default Author;
