import React from 'react';
import { Link } from 'gatsby';

const NavLink = ({ to, children }) => {
  return (
    <Link to={to}>
      <span className="text-white font-semibold ml-4">{children}</span>
    </Link>
  );
};

export default NavLink;

