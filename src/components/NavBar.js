import * as React from 'react';
import Container from './Container';
import { Link } from 'gatsby';
import NavLink from '../components/NavLink'

const NavBar = () => {
    return (
        <div className="fixed top-0 bg-slate-600 px-6 py-3 w-full">
        <Container>
          <div className="flex items-center justify-between">
            <div className="text-sm text-white font-bold">
            <Link to="/">
              <img src="https://www.datocms-assets.com/102950/1687280044-346046480_2336438823204493_9136256174124928094_n.jpg" alt="Company Logo" className="h-8 rounded-full" />
            </Link>
            </div>
             <div className="flex items-center justify-between">
              <NavLink to="/all-articles">All Articles</NavLink>
              <NavLink to="/about-us">About</NavLink>
              <NavLink to="/contact">Contact</NavLink>
              </div>
          </div>
        </Container>
      </div>
    )
}

export default NavBar;  