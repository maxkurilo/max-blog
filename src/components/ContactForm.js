import * as React from 'react';

const ContactForm = () => {
  return (
    <form className="mt-8">
          <label className="block mb-2" htmlFor="name">Name:</label>
          <input className="w-full p-2 mb-4 border" type="text" id="name" name="name" required />

          <label className="block mb-2" htmlFor="email">Email:</label>
          <input className="w-full p-2 mb-4 border" type="email" id="email" name="email" required />

          <label className="block mb-2" htmlFor="message">Message:</label>
          <textarea className="w-full p-2 mb-4 border" id="message" name="message" rows="4" required></textarea>

          <button className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-700" type="submit">Send Message</button>
    </form>
  );
};

export default ContactForm;
